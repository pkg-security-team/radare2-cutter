radare2-cutter (1.12.0-2) unstable; urgency=medium

  [ Arnaud Rebillout ]
  * Team upload
  * Add patches to build against radare2 5.5
  * Closes: #1009173 #1011350

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 29 Aug 2022 11:49:34 +0200

radare2-cutter (1.12.0-1) unstable; urgency=medium

  * New upstream version 1.12.0 (Closes: #976847)
  * d/control: add build dep qttools5-dev

 -- Robert Haist <rha@debian.org>  Mon, 25 Jan 2021 10:18:20 +0100

radare2-cutter (1.10.2-1) unstable; urgency=medium

  * New upstream version 1.10.2

 -- Robert Haist <rha@debian.org>  Tue, 10 Mar 2020 11:16:27 +0100

radare2-cutter (1.10.1-3) unstable; urgency=medium

  * d/control: make supported architectures explicit
  * d/rules: set MAKE_BUILD_TYPE to remove debug fuzz
  * d/rules: export harding flags like suggested in the docs
  * d/rules: make crash reports opt-in (privacy)

 -- Robert Haist <rha@debian.org>  Thu, 27 Feb 2020 14:20:23 +0100

radare2-cutter (1.10.1-2) unstable; urgency=medium

  * d/control: Add dh-strip-nondeterminism to build reproducible
  * d/control: Point to the correct Cutter website (Closes: #921391)
  * This release and libradare2 in sid closes: #931172

 -- Robert Haist <rha@debian.org>  Wed, 26 Feb 2020 12:51:37 +0100

radare2-cutter (1.10.1-1) unstable; urgency=medium

  [ Hilko Bengen ]
  * New upstream version 1.8.0
  * New upstream version 1.8.1

  [ Samuel Henrique ]
  * Add salsa-ci.yml
  * Configure git-buildpackage for Debian

  [ Robert Haist ]
  * New upstream version 1.10.1
  * Added python support
  * Added syntax highlighting support
  * Added graphviz support
  * Bump standards version
  * Move to debhelper-compat

 -- Robert Haist <rha@debian.org>  Tue, 18 Feb 2020 23:02:09 +0100

radare2-cutter (1.7.4-2) unstable; urgency=medium

  * Fix installation from build directory, fixing FTBFS on i386

 -- Hilko Bengen <bengen@debian.org>  Mon, 04 Feb 2019 12:01:29 +0100

radare2-cutter (1.7.4-1) unstable; urgency=low

  * Team upload
  * Initial release. (Closes: #910850)

 -- Hilko Bengen <bengen@debian.org>  Sat, 26 Jan 2019 00:54:03 +0100
